from django.contrib import admin
from apps.propietario.models import Propietario

# Register your models here.
admin.site.register(Propietario)
