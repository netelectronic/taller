from django.db import models

class Propietario(models.Model):
    nombre = models.CharField(max_length = 50)
    apellido= models.CharField(max_length = 50)
    sexo = models.CharField(max_length = 10)
    telefono = models.CharField(max_length = 10)
    email  = models.EmailField()
    fecha_ingreso = models.DateField()
    domicilio = models.TextField() 


