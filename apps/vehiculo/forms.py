from django import forms

from apps.vehiculo import Vehiculo


class VehiculoForm(forms.ModelForm):
    class Meta:
        model = Vehiculo
        fields = [
            'placa',
            'marca',
            'modelo',
            'cilindros',
            'propietario',
            'orden',
        ]
        labels = {
            'placa': 'Placa',
            'marca': 'Marca',
            'modelo': 'Modelo',
            'cilindros': 'Cilindros',
            'propietario': 'Propietario',
            'orden': 'Orden',

        }
        widgets = {
            'placa': forms.TextInput(attrs={'class':'forms-controls'}),
            'marca': forms.TextInput(attrs={'class':'forms-controls'}),
            'modelo': forms.TextInput(attrs={'class':'forms-controls'}),
            'cilindros': forms.TextInput(attrs={'class':'forms-controls'}),
            'propietario': forms.Select(attrs={'class':'forms-controls'}),
            'orden': forms.CheckboxSelectMultiple(),


        }

