from django.db import models
from apps.propietario.models import Propietario

class Orden (models.Model):
    tiporden = models.CharField(max_length = 20)


class Vehiculo (models.Model):
    placa = models.CharField(max_length = 10)
    marca = models.CharField(max_length = 15)
    modelo = models.CharField(max_length = 15)
    cilindros = models.IntegerField(default=0)
    propietario = models.ForeignKey(Propietario, null = True, blank=True, on_delete =  models.CASCADE)
    orden = models.ManyToManyField(Orden, blank = True)